import {createSlice} from "@reduxjs/toolkit";

const productsReducer = createSlice({
    name: 'products',
    initialState: {
        products: [
            {
                id: 0,
                image: "https://c.dns-shop.ru/thumb/st1/fit/500/500/2ba75d1cbb16a04d7ff689fad4756c50/3c5fc580c9f6591fa59140dd574c28162a1e8d559f82ae135de942df2d10048f.jpg.webp",
                title: "Lorem ipsum dolor sit amet",
                description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat",
                price: 155,
            },
            {
                id: 1,
                image: "https://c.dns-shop.ru/thumb/st1/fit/500/500/822a94e22d3044fd3c35e164aff8c28c/ff926a433d32084bab5221d334cc0824732b1e9e988b61ede4a7e23494c205fe.jpg.webp",
                title: "But I must explain",
                description: "or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences",
                price: 700,
            },
            {
                id: 2,
                image: "https://c.dns-shop.ru/thumb/st4/fit/500/500/2b0813e9c2b8b7c28ce6809ebbefc64d/8ccd2e3923055be178467f1eaa0d32320435e1952f4f0759d176cead08216b22.jpg.webp",
                title: "At vero eos et accusamus",
                description: "that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because",
                price: 700,
            },
            {
                id: 3,
                image: "https://c.dns-shop.ru/thumb/st4/fit/500/500/d647df37fac6d06396e9d25fd23adc30/bbbd89e278c7e0f1bc3532af1ce703f138338fb1a564d29ac907ed7a5597942b.jpg.webp",
                title: "On the other hand",
                description: "occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who",
                price: 700,
            },{
                id: 4,
                image: "https://c.dns-shop.ru/thumb/st1/fit/500/500/1871764329144637367b278747854bc2/9c20c36e7475754381d18dbfbc4496400d9d906c9ced3320977f0aa6107cf73c.jpg.webp",
                title: "Quidem rerum facilis",
                description: "Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus",
                price: 700,
            },

        ]
    },

})

export default productsReducer.reducer
