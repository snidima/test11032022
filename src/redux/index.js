import { configureStore } from "@reduxjs/toolkit"
import cartReducer from './cartReducer'
import productsReducer from './productsReducer'

export const store = configureStore({
    reducer:{
        cart: cartReducer,
        products: productsReducer
    }
})
