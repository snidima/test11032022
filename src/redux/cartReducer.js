import {createSlice} from "@reduxjs/toolkit";

const cartReducer = createSlice({
    name: 'cart',
    initialState: {
        items: []
    },

    reducers:{
        add(state, action){
            if( state.items.filter( item => item.id === action.payload.id ).length > 0 ){
                state.items = state.items.map( item => {
                    if( item.id === action.payload.id ){
                        item.quantity+= action.payload.quantity
                    }
                    return item
                })
            } else {
                state.items.push( action.payload )
            }
        },

        changeQuantity(state, action){
            const { id, quantity } = action.payload

            state.items = state.items.map( item => {
                if( item.id === id ){
                    item.quantity = quantity
                }
                return item
            } ).filter( item => item.quantity > 0 )
        }
    }
})

export const { add, remove, changeQuantity } = cartReducer.actions
export default cartReducer.reducer
