import Card from "./Card";
import React from "react";
import { useSelector } from "react-redux"

function Cards() {
    const products = useSelector(state => state.products.products)

    return (
        <div className="row cards">
            { products.map( product =>
                <div className="col-sm-6 col-md-4 d-flex pb-3" key={product.id}>
                    <Card item={product} />
                </div>
            )}
        </div>
    )
}

export default Cards;
