import React from "react"
import Modal from 'react-modal'
import { useSelector } from "react-redux"
import { useDispatch } from "react-redux"
import { changeQuantity } from "../redux/cartReducer";
import {NotificationManager} from "react-notifications";

Modal.setAppElement('#app');

function Navigation() {

    const dispatch = useDispatch()
    const cartItems = useSelector(state => state.cart.items)
    const products = useSelector(state => state.products.products)


    const [modalIsOpen, setIsOpen] = React.useState(false);

    function openModal() {
        setIsOpen(true);
    }

    function closeModal() {
        setIsOpen(false);
    }

    const getProductById = ( id ) => {
        return products.filter( product => product.id === id ).pop()
    }

    const productsCartMap = () => {
        return cartItems.map( item => {
            const product = getProductById( item.id )
            return {...item, product, sum: product.price * item.quantity }
        } )
    }

    const totalSum = () => productsCartMap().reduce( (acc, item) => acc + item.sum, 0 )
    const totalQuantity = () => productsCartMap().reduce( (acc, item) => acc + item.quantity, 0 )

    const changeQuantityHandler = ( id, quantity ) => {
        dispatch( changeQuantity({ id, quantity } ) )
        if( quantity === 0 ){
            NotificationManager.error(`${getProductById(id).title} have been delete from cart`);
        }
    }

    return(
        <div className="navigation">
            <div style={{textAlign: "right"}}>
                <button disabled={cartItems.length <= 0} onClick={openModal} className="btn btn-primary">
                    <i className="bi bi-cart4"></i> Cart {totalSum() > 0 && <b>{totalSum()}$</b>}
                </button>
            </div>
            <Modal isOpen={modalIsOpen}>
                <div className="modal-close-btn" onClick={closeModal}>
                    <i className="bi bi-x-circle"></i>
                </div>
                <h2>Cart</h2>
                {cartItems.length <= 0 ? (
                    <div className="empty-cart-wrapper">
                        <h1>Cart is empty</h1>
                    </div>
                ) : (
                <table className="table cart-table ">
                    <thead>
                        <tr>
                            <th>Code</th>
                            <th>Title</th>
                            <th style={{textAlign: "center"}}>Image</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Sum</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                    { productsCartMap().map( item =>
                        <tr key={item.id}>
                            <th>#{item.product.id}</th>
                            <td>{item.product.title}</td>
                            <td><img className="cart-table-image-wrapper" src={item.product.image} /></td>
                            <td>{item.product.price}</td>
                            <td>
                                <div className="btn-group">
                                    <button className="btn btn-light" disabled={item.quantity <= 1 } onClick={ () => changeQuantityHandler(item.id, item.quantity*1 - 1)}>-</button>
                                    <button disabled={true} className="btn btn-light"><b>{item.quantity}</b></button>
                                    <button className="btn btn-light" onClick={ () => changeQuantityHandler(item.id, item.quantity*1 + 1)}>+</button>
                                </div>
                            </td>
                            <td>{item.sum}</td>
                            <td><i onClick={ () => changeQuantityHandler(item.id, 0)} className="bi bi-trash btn btn-danger"></i></td>
                        </tr>
                    )}
                    <tr>
                        <td colSpan="7" style={{textAlign: "right"}}>
                            <b>Total sum: {totalSum()}$</b>
                        </td>
                    </tr>
                    <tr>
                        <td colSpan="7" style={{textAlign: "right"}}>
                            <b>Products quantity: {cartItems.length}</b>
                        </td>
                    </tr>
                    <tr>
                        <td colSpan="7" style={{textAlign: "right"}}>
                            <b>Total quantity: {totalQuantity()}</b>
                        </td>
                    </tr>
                    </tbody>
                </table>
                )}
            </Modal>
        </div>
    )
}

export default Navigation
