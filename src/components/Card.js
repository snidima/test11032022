import React, { useState } from 'react'
import { useDispatch } from "react-redux"
import { add } from "../redux/cartReducer";
import {NotificationManager} from "react-notifications";


function Card({item}) {

    const [count, setCount] = useState(1)
    const dispatch = useDispatch()

    const inc = () => {
        if( count >= 1000 ) return
        setCount( count + 1 )
    }
    const dec = () => {
        if( count < 2 ) return
        setCount( count - 1 )
    }

    const addToCart = () => {
        dispatch( add({
            id: item.id,
            quantity: count
        }) )
        NotificationManager.info(`${count} items have been added to cart`);
    }

    return (
        <div className="card" >
            <img src={item.image} className="card-img-top" />
            <div className="card-body">
                <h5 className="card-title">{item.title}, #{item.id} <span className="badge bg-dark">{item.price}$</span></h5>
                <p className="card-text">{item.description}</p>
                <div className="mb-3">
                    <input type="number" className="form-control" value={count} onChange={e => setCount( e.target.value )} />
                </div>
                <div className="btn-group">
                    <button className="btn btn-light" onClick={dec} disabled={count <=1 }>-</button>
                    <button className="btn btn-light" onClick={inc} disabled={count >= 1000}>+</button>
                    <button onClick={addToCart} className="btn btn-primary" disabled={count < 1 || count > 1000}>
                        <i className="bi bi-cart-plus"></i> Add {count}
                    </button>
                </div>
            </div>
        </div>


    )
}

export default Card;
