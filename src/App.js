import React from 'react'
import Cards from './components/Cards'
import Navigation from './components/Navigation'

import 'react-notifications/lib/notifications.css';
import { NotificationContainer } from 'react-notifications';

function App() {
    return (
        <div className="App container">
            <Navigation/>
            <Cards/>
            <NotificationContainer/>
        </div>
    )
}

export default App;
