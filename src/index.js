import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux"
import { store } from "./redux"

import 'bootstrap/dist/css/bootstrap.min.css'

import 'bootstrap-icons/font/bootstrap-icons.css'
import 'bootstrap-icons/font/fonts/bootstrap-icons.woff2'
import 'bootstrap-icons/font/fonts/bootstrap-icons.woff'

import './index.css'
import App from './App';


ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>,
  document.getElementById('app')
);
